var height = $(window).height(),
	srcHeight = document.body.scrollHeight,
	width = $(window).width(),
	// group,
	deg = 0,
	flipDeg = 0,
	logoColorIdx = 256,
	logoColorSwitch = 1,
	$rocket = $("#rocket-svg"),
	scaleVel = 0.01;
logoColor = logoColorIdx - logoColorSwitch;
progressBar = $(".progress-bar-container");


// console.log(Evolution.hasShortCut);





$(function () {
	init();
});

function init() {
	bgEffect();
	waveEffect();
	updateProgressBar();
	setInterval(() => {
		rotateLogo(),
			changeColorLogo(),
			changeColorRocketDots();
	}, 25);
	setInterval(() => {
		changeColorFlame();
	}, 100);
	
	landingProgress();
	
}



var bgEffect = () => {
	$(".page-1").hide().fadeIn(2000);
}

var waveEffect = () => {
	$('#feel-the-wave').wavify({
		height: 60,
		bones: 4,
		amplitude: 15,
		color: '#1A237E',
		speed: .15
	});

	$('#feel-the-wave-two').wavify({
		height: 60,
		bones: 3,
		amplitude: 15,
		color: 'rgba(26, 35, 126, .8)',
		speed: .25
	});
}

var updateProgressBar = () => {
	progressBar.hide();
	$(window).scroll(function () {
		let currTop = $(window).scrollTop(),
			heightOffset = srcHeight - height,
			progress = (currTop / heightOffset) * 100;

		if (progress == 0) {
			progressBar.slideUp('slow');
		} else if (progress > 10 && progress < 90)
			$(".nav-container").slideUp("slow");
		else if (progress < 5 || progress > 95) {
			$(".nav-container").slideDown("slow");
			progressBar.slideDown();
		}
		progressBar.css("width", progress + "%");

		if (progress >= 20 && progress <= 30)
			ability();
	});
}

var rotateLogo = () => {
	$("#logo-svg").css("transform", "rotate(" + deg + "deg)");
	++deg;
	if (deg == 360)
		deg = 1;
}

var changeColorLogo = () => {
	$("#logo-svg path").css("fill", "rgb(" + logoColor + ", 0, 0)");
	logoColor -= logoColorSwitch;
	if (logoColor == 127 || logoColor == 256)
		logoColorSwitch *= -1;
}

var changeColorRocketDots = () => {
	let redColor = "rgb(" + logoColor + ", 0, 0)",
		yellowColor = "rgb(255 ," + logoColor + ", 0)",
		dots = ["#top-red-dot", "#left-red-dot", "#right-red-dot"];

	dots.forEach(elem => {
		if (logoColorSwitch > 0)
			$(elem, $rocket).attr('style', "fill:" + redColor);
		else {
			$(elem, $rocket).attr('style', "fill:" + yellowColor);
		}
	});
}

var changeColorFlame = () => {
	let flames = ["#red-flame", "#yellow-flame", "#orange-flame"],
		colors = ["fill:url(#linearGradient3796)", "fill:url(#linearGradient1729)", "fill:url(#linearGradient1727)"];

	flames.forEach(elem => {
		$(elem, $rocket).attr('style', colors[random(0, 2)]);
	})
}
var landingProgress = () => {
	var obj = {
		landing: "0%"
	};
	var JSobject = anime({
		targets: obj,
		landing: "100%",
		duration: 15000,
		round: 1,
		easing: 'easeOutQuad',
		update: function () {
			var el = document.querySelector('.javascript');
			el.innerHTML = "rocket.landing(" + JSON.stringify(obj.landing) + ")";
			reInitHighlight();
		}
	});
}
var reInitHighlight = () => {
	hljs.initHighlighting.called = false;
	hljs.initHighlighting();
}
function random(min, max) {
	var num = Math.floor(Math.random() * (max - min + 1)) + min;
	return num;
}

var ability = () => anime({
	targets: "#ablity polymorph",
	points: [{
			value: '70 41 118.574 59.369 111.145 132.631 60.855 84.631 20.426 60.369'
		},
		{
			value: '70 6 119.574 60.369 100.145 117.631 39.855 117.631 55.426 68.369'
		},
		{
			value: '70 57 136.574 54.369 89.145 100.631 28.855 132.631 38.426 64.369'
		},
		{
			value: '70 24 119.574 60.369 100.145 117.631 50.855 101.631 3.426 54.369'
		}
	],
	easing: 'easeOutQuad',
	duration: 2000,
	loop: true
});



var backAndForth = (targetElement, speed) => {
	$(targetElement).animate({
		left: "+=250px"
	}, {
		duration: speed,
		complete: () => {
			targetElement.animate({
				right: "-=250px"
			}, {
				duration: speed,
				complete: () => {
					backAndForth(targetElement, speed);
				}
			});
		}
	});
};